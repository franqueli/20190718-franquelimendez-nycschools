//
//  SchoolDataSource.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/19/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import Foundation

protocol SchoolDataSourceDelegate: class {
    func datasource(_ datasource:SchoolDataSource, startedWithInfo info: [String: Any])
    func datasource(_ datasource:SchoolDataSource, finishedWithInfo info: [String: Any])
    func datasource(_ datasource:SchoolDataSource, failedWithInfo info: [String: Any])
}

class SchoolDataSource {
    weak var delegate: SchoolDataSourceDelegate?
    var dataTask: URLSessionDataTask?

    // Subclass override and provide the correct url
    func apiURLString() -> String {
        return ""
    }
    
    func reload() {
        // Cancel prior request
        if let dataTask = dataTask {
            if dataTask.state == URLSessionTask.State.running {
                dataTask.cancel()
            }
        }

        let url = URL(string: apiURLString())

        if let url = url {
            dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print("*** API Call Failed: \(error)");
                    self.publishFailed(["error": error])
                    return;
                }

                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("*** API Failed Code: \(httpResponse.statusCode)")
                        self.publishFailed(["error": "Bad http status: \(httpResponse.statusCode)"])
                        return;
                    }
                }

                if let data = data {
                    self.processResponseData(responseData: data)
                }
            }

            publishStarted([:])
            dataTask?.resume()
        }
    }

    // Subclass must override to process response data
    func processResponseData(responseData: Data) {
        DispatchQueue.main.sync { () -> Void in
            let dataStr = String(data: responseData, encoding: .utf8)

            print("\(dataStr ?? "")")
        }
    }

    // These shouldn't be part of the public API
    func publishStarted (_ info: [String: Any]) {
        DispatchQueue.main.async { () -> Void in
            self.delegate?.datasource(self, startedWithInfo: info)
        }
    }

    func publishFinished (_ info: [String: Any]) {
        DispatchQueue.main.async { () -> Void in
            self.delegate?.datasource(self, finishedWithInfo: info)
        }
    }

    func publishFailed (_ info: [String: Any]) {
        DispatchQueue.main.async { () -> Void in
            self.delegate?.datasource(self, failedWithInfo: info)
        }
    }


}
