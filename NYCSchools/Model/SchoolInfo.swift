//
//  SchoolInfo.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/18/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import Foundation


struct SchoolInfo: Decodable {
    var dbn: String              // Key used to cross reference with other data associated with a school
    var name: String
    var phoneNumber: String
    var schoolEmail: String?
    var website: String
    var boro: String           // Map to enum value
    var latitude: String?      // This should be an Int but the json defines this as a string
    var longitude: String?     // This should be an Int but the json defines this as a string
    var addressLine1: String   // primary_address_line_1
    var state: String          // state_code
    var city: String           // city
    var postCode: String       // zip
    var numberOfStudents: String  // "total_students"

    var boroughName : String {
        get {
            switch boro {
            case "M":
                 return "Manhattan"
            case "K":
                return "Brooklyn"
            case "X":
                return "Bronx"
            case "Q":
                return "Queens"
            case "S":
                return "Staten Island"
            default:
                return ""
            }
        }
    }

    var schoolSATInfo: SchoolSATInfo?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case boro
        case latitude
        case longitude
        case addressLine1 = "primary_address_line_1"
        case state = "state_code"
        case city
        case postCode = "zip"
        case numberOfStudents = "total_students"        
    }
    
}
