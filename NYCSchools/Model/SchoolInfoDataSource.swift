//
//  SchoolInfoDataSource.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/18/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import Foundation

class SchoolInfoDataSource: SchoolDataSource {
  
    var data:[SchoolInfo] = []

    override func apiURLString () -> String {
        return "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    }

    // For testing locally
//    func loadData() {
//        data = [SchoolInfo]()
//
//        let path = Bundle.main.url(forResource: "NYCHighSchools", withExtension: "json")
//
//        if let path = path {
//            do {
//                let json = try Data.init(contentsOf: path)
//                let decoder = JSONDecoder()
//                data = try decoder.decode([SchoolInfo].self, from: json)
//
//            } catch {
//                print("\(error)")
//            }
//        }
//    }

    override func processResponseData (responseData: Data) {
        do {
            let decoder = JSONDecoder()
            data = try decoder.decode([SchoolInfo].self, from: responseData)

            publishFinished(["data": data])
//            print("SchoolInfo*** \(data) ***")
        } catch {
            publishFailed(["error": error])
//            print("\(error)")
        }
    }
}
