//
//  SchoolSATInfo.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/18/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import Foundation

struct SchoolSATInfo: Decodable {
    var dbn: String
    var schoolName: String
    var numberOfTestTakers: String                // This should be an Int type but json returns a string type. Need custom parsing to convert to Int
    var readingAvgScore: String          // Json returns string but this should be an Int type
    var mathAvgScore: String             // Json returns string but this should be an Int type
    var writingAvgScore: String          // Json returns string but this should be an Int type
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
    
}
