//
//  SchoolSATInfoDataSource.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/18/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import Foundation

class SchoolSATInfoDataSource : SchoolDataSource {

    var satInfo:[SchoolSATInfo] = []

    override func apiURLString () -> String {
        return "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }

    // For testing locally
//    func loadData() {
//        let path = Bundle.main.url(forResource: "NYCHighSchoolsSATInfo", withExtension: "json")
//
//        if let path = path {
//            do {
//                let json = try Data.init(contentsOf: path)
//                let decoder = JSONDecoder()
//                satInfo = try decoder.decode([SchoolSATInfo].self, from: json)
//
////                print("**** \(satInfo) ****")
//            } catch {
//                print("\(error)")
//            }
//        }
//    }

    override func processResponseData (responseData: Data) {
        do {
            let decoder = JSONDecoder()
            satInfo = try decoder.decode([SchoolSATInfo].self, from: responseData)

            publishFinished(["data": satInfo])
//            print("SchoolSATInfo**** \(satInfo) ****")
        } catch {
            publishFailed(["error": error])
//            print("\(error)")
        }
    }

}
