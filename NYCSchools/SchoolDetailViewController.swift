//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/20/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var schoolLabel: UILabel!

    private var valuesToShow:[String] = []

    var detailItem: SchoolInfo? {
        didSet {
            valuesToShow.removeAll()

            if let website = detailItem?.website {
                valuesToShow.append(website)
            }

            if let phone = detailItem?.phoneNumber {
                valuesToShow.append(phone)
            }

            if let addr1 = detailItem?.addressLine1 {
                valuesToShow.append(addr1)
            }

            if let city = detailItem?.city {
                valuesToShow.append(city)
            }

            if let state = detailItem?.state {
                valuesToShow.append(state)
            }

            if let postCode = detailItem?.postCode {
                valuesToShow.append(postCode)
            }

            // Update the view.
            configureView()
        }
    }

    func configureView() {
        if !self.isViewLoaded {
            return
        }

        // Update the user interface for the detail item.
        if let detail = detailItem {
            schoolLabel.text = detail.name
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "School Detail"

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismiss(_:)))
        navigationItem.rightBarButtonItem = cancelButton

        let mapButton = UIBarButtonItem(title: "Map", style: .plain, target: self, action: #selector(map(_:)))        
        navigationItem.leftBarButtonItem = mapButton

        configureView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CELL")
    }

    @objc
    func dismiss(_ sender: Any?) {
        self.dismiss(animated: true)
    }

    @objc
    func map(_ sender: Any?) {
        let mapViewController = SchoolMapViewController()
        mapViewController.detailItem = detailItem

        self.navigationController?.pushViewController(mapViewController, animated: true)
    }


}

extension SchoolDetailViewController: UITableViewDataSource, UITableViewDelegate {
    public func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return valuesToShow.count
    }

    public func tableView (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)

        cell.textLabel?.text = valuesToShow[indexPath.row]


        return cell
    }
}