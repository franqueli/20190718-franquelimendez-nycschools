//
//  MasterViewController.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/18/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import UIKit

class SchoolListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    private var loadingView: UIView!

    private var detailViewController: SchoolSATInfoViewController? = nil
    private var schools: [SchoolInfo]!
    private var schoolSATScores: [String: SchoolSATInfo]!

    // These should be batched into their own class
    private var finishedCount: Int = 0
    private var schoolInfoDataSource: SchoolInfoDataSource!
    private var schoolSATInfoDataSource: SchoolSATInfoDataSource!
    private var alertVC: UIAlertController?

    var sortAscending: Bool = true
    var selectedSort: SchoolSortOption! {
        didSet {
            switch selectedSort! {
            case .default:
                print("Default sort")
                schools.sort { (schoolInfo1, schoolInfo2) -> Bool in
                    if sortAscending {
                        return schoolInfo1.name < schoolInfo2.name
                    } else {
                        return schoolInfo1.name > schoolInfo2.name
                    }
                }
            case .borough:
                print("borough sort")                
                schools.sort { (schoolInfo1, schoolInfo2) -> Bool in
                    if sortAscending {
                        return schoolInfo1.boroughName < schoolInfo2.boroughName
                    } else {
                        return schoolInfo1.boroughName > schoolInfo2.boroughName
                    }
                }
            case .numberOfStudents:
                print("numberOfStudents")                
                schools.sort { (schoolInfo1, schoolInfo2) -> Bool in
                    let school1Students = Int(schoolInfo1.numberOfStudents) ?? 0
                    let school2Students = Int(schoolInfo2.numberOfStudents) ?? 0
                    if sortAscending {
                        return school1Students < school2Students
                    } else {
                        return school1Students > school2Students
                    }
                }
            @unknown default:
                schools.sort { (schoolInfo1, schoolInfo2) -> Bool in
                    if sortAscending {
                        return schoolInfo1.numberOfStudents < schoolInfo2.numberOfStudents
                    } else {
                        return schoolInfo1.numberOfStudents > schoolInfo2.numberOfStudents
                    }
                }
            }
        }
    }


    override func viewDidLoad () {
        super.viewDidLoad()

        self.title = "NYC Schools"

        addLoadingView()

        let sortButton = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(showSort(_:)))
        navigationItem.rightBarButtonItem = sortButton

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count - 1] as! UINavigationController).topViewController as? SchoolSATInfoViewController
        }

        loadData()

        self.tableView.reloadData()
    }

    override func viewWillAppear (_ animated: Bool) {
        super.viewWillAppear(animated)

        if splitViewController!.isCollapsed {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                tableView.deselectRow(at: selectedIndexPath, animated: false)
            }
        }
    }


    func addLoadingView() {
        let loadingView = UIView(frame: CGRect(x: 0, y: 0, width: 250.0, height: 100.0))
        loadingView.layer.cornerRadius = 5.0
        loadingView.backgroundColor = UIColor(white: 0.5 , alpha: 0.5)                   // Gray background
        let label = UILabel()
        label.text = "Loading..."
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.textAlignment = .center

        let stackView = UIStackView(arrangedSubviews: [label])
        stackView.frame = loadingView.bounds

        loadingView.addSubview(stackView)

        self.view.addSubview(loadingView)
        loadingView.center = self.view.center
        self.view.bringSubviewToFront(loadingView)

        self.loadingView = loadingView

        hideLoadingView()
    }

    func showLoadingView() {
        self.loadingView.isHidden = false
    }

    func hideLoadingView() {
        self.loadingView.isHidden = true
    }

    // Call to service
    private func loadData() {
        finishedCount = 0;
        schoolInfoDataSource = SchoolInfoDataSource()
        schoolInfoDataSource.delegate = self
        schoolInfoDataSource.reload()

        schoolSATInfoDataSource = SchoolSATInfoDataSource()
        schoolSATInfoDataSource.delegate = self
        schoolSATInfoDataSource.reload()
    }

    // Called when both datasources complete successfully
    private func processFinishedDataSources() {
        self.schools = schoolInfoDataSource.data

        var satInfoMapping = [String: SchoolSATInfo]()
        for satInfo in schoolSATInfoDataSource.satInfo {
            satInfoMapping[satInfo.dbn] = satInfo
        }

        self.schoolSATScores = satInfoMapping

        self.tableView.reloadData()
    }

    @objc
    func showSort (_ sender: Any) {
        let sortViewController = SchoolSortOptionsViewController()
        let navController = UINavigationController(rootViewController: sortViewController)
        sortViewController.delegate = self

        self.present(navController, animated: true)
    }

    // MARK: - Segues

    override func prepare (for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                var school = schools[indexPath.row]
                school.schoolSATInfo = schoolSATScores[school.dbn]

                let controller = (segue.destination as! UINavigationController).topViewController as! SchoolSATInfoViewController

                controller.detailItem = school
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }


    func sortBy (_ option: SchoolSortOption, ascending: Bool) {
        sortAscending = ascending
        selectedSort = option

        tableView.reloadData()
    }

    // MARK: - Table View

    func numberOfSections (in tableView: UITableView) -> Int {
        return 1
    }

    func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools?.count ?? 0
    }

    func tableView (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if let schoolInfo = schools?[indexPath.row] {
            cell.textLabel!.text = schoolInfo.name
            cell.detailTextLabel!.text = "Students: \(schoolInfo.numberOfStudents) - \(schoolInfo.boroughName)"
        }

        return cell
    }

    func tableView (_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if let schoolInfo = schools?[indexPath.row] {
            let schoolDetailViewController = SchoolDetailViewController()
            schoolDetailViewController.detailItem = schoolInfo
            let navController = UINavigationController(rootViewController: schoolDetailViewController)

            present(navController, animated: true)
        }
    }

}

extension SchoolListViewController: SchoolDataSourceDelegate {
    func datasource (_ datasource: SchoolDataSource, startedWithInfo info: [String: Any]) {
        showLoadingView()
    }

    func datasource (_ datasource: SchoolDataSource, finishedWithInfo info: [String: Any]) {
        finishedCount += 1

        if (finishedCount == 2) {
            hideLoadingView()
            processFinishedDataSources()
        }
    }

    func datasource (_ datasource: SchoolDataSource, failedWithInfo info: [String: Any]) {
        hideLoadingView()
        // Alert the user give them the option to retry or cancel

        var allowRetry = true
        var errorMessage = "Service call failed. Tap ok to try again"
        let error = info["error"]

        if let error = error as? NSError {
            if (error.code == NSURLErrorNotConnectedToInternet) {
                allowRetry = false
                errorMessage = error.localizedDescription
            }
        }

        self.alertVC = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        if allowRetry {
            let defaultAction = UIAlertAction(title: "OK", style: .default) { action in
                self.loadData()
            }

            alertVC?.addAction(defaultAction)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC?.addAction(cancelAction)

        if let alertVC = alertVC {
            self.present(alertVC, animated: true)
        }
    }


}
