//
//  SchoolMapAnnotation.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/20/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import Foundation

import MapKit

class SchoolMapAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?

    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}
