//
//  SchoolMapViewController.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/20/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import UIKit
import MapKit


class SchoolMapViewController: UIViewController {
    let regionRadius: CLLocationDistance = 2500

    @IBOutlet var mapView: MKMapView!

    var detailItem: SchoolInfo? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    func configureView() {
        if !self.isViewLoaded {
            return
        }

        // Update the user interface for the detail item.
        if let detail = detailItem, let latitude = detail.latitude, let longitude = detail.longitude {
            let lat = Double(latitude) ?? 0
            let lon = Double(longitude) ?? 0
            if lat != 0 && lon != 0 {
                let coordinate = CLLocation(latitude: lat, longitude: lon)
                centerMapOnLocation(location: coordinate)

                let coordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                let annotation = SchoolMapAnnotation(coordinate: coordinate2D, title: detail.name, subtitle: detail.website)
                mapView.addAnnotation(annotation)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "School Map"

        let initialLocation = CLLocation(latitude: 40.78343, longitude: -73.96625)    // Manhattan

        centerMapOnLocation(location: initialLocation)

        configureView()
    }

    func centerMapOnLocation (location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }


}
