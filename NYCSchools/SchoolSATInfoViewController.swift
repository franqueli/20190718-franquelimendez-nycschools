//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/18/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import UIKit

class SchoolSATInfoViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var criticalReadingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var testTakersLabel: UILabel!

    @IBOutlet weak var testLabelContainerView: UIView!
    @IBOutlet weak var unavailableInfoLabel: UILabel!

    var detailItem: SchoolInfo? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    func configureView() {
        if !self.isViewLoaded {
            return
        }
        
        testLabelContainerView.isHidden = false
        unavailableInfoLabel.isHidden = true

        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.name
                if let satInfo = detail.schoolSATInfo {
                    criticalReadingScoreLabel.text = satInfo.readingAvgScore
                    mathScoreLabel.text = satInfo.mathAvgScore
                    writingScoreLabel.text = satInfo.writingAvgScore
                    testTakersLabel.text = "Test Takers: \(satInfo.numberOfTestTakers)"
                } else {
                    testLabelContainerView.isHidden = true
                    unavailableInfoLabel.isHidden = false
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }



}

