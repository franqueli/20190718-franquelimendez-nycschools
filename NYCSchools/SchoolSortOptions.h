//
//  SchoolSortOptions.h
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/19/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SchoolSortOption) {
    SchoolSortOptionDefault = 0,
    SchoolSortOptionBorough,
    SchoolSortOptionNumberOfStudents
};

@interface SchoolSortOptions : NSObject

+ (nonnull NSString *) nameForSortOption:(SchoolSortOption) sortOptions;

@end
