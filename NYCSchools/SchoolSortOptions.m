//
//  SchoolSortOptions.m
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/19/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SchoolSortOptions.h"

@implementation SchoolSortOptions

+ (NSString *) nameForSortOption:(SchoolSortOption) sortOptions{
    NSString *sortOptionName = @"";
    
    switch (sortOptions) {
        case SchoolSortOptionDefault:
            sortOptionName = @"School Name";
            break;
        case SchoolSortOptionBorough:
            sortOptionName = @"Borough";
            break;
        case SchoolSortOptionNumberOfStudents:
            sortOptionName = @"Number of Students";
            break;
    }
    
    return sortOptionName;
}

@end
