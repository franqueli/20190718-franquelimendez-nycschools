//
//  SchoolSortOptionsViewController.swift
//  NYCSchools
//
//  Created by Franqueli Mendez on 7/19/19.
//  Copyright © 2019 Franqueli Mendez. All rights reserved.
//

import UIKit

class SchoolSortOptionsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var sortOptions: [SchoolSortOption] = [.default, .borough, .numberOfStudents]

    weak var delegate: SchoolListViewController?                          // FIXME: We should define a protocol for this instead of a concrete VC reference
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismiss(_:)))

        navigationItem.rightBarButtonItem = cancelButton

        tableView.register(SortOptionTableViewCell.self, forCellReuseIdentifier: "Cell")
    }

    @objc
    func dismiss(_ sender: Any?) {
        self.dismiss(animated: true)
    }

}

extension SchoolSortOptionsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        var name = SchoolSortOptions.name(for: sortOptions[indexPath.row])

        let sortAscending = delegate?.sortAscending ?? true

        if sortOptions[indexPath.row] == delegate?.selectedSort {
            name = name + (sortAscending ? " (ASC)" : " (DESC)")
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17.0)
        } else {
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17.0)
        }
        
        cell.textLabel?.text = name
        
        return cell
    }

}

extension SchoolSortOptionsViewController: UITableViewDelegate {
    public func tableView (_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var sortAscending = true

        if let masterVC = delegate {
            if (masterVC.selectedSort == sortOptions[indexPath.row]) {
                sortAscending = !masterVC.sortAscending                  // Toggle sort direction
            }
            masterVC.sortBy(sortOptions[indexPath.row], ascending: sortAscending)
        }

        self.dismiss(animated: true)
    }
}
