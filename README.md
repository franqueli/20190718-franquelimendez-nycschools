Coding Challenge NYC Schools

This project was built with XCode 10.2.1 and targets iOS 12.2

The application uses data from the cityofnewyork.us 

Service URLs<br>
School Information  https://data.cityofnewyork.us/resource/s3k6-pzi2.json<br>
School SAT Info https://data.cityofnewyork.us/resource/f9bf-2cp4.json

Main View Controllers<br>
SchoolListViewController - Initial view controller displays the list of NYC Schools<br>
SchoolSATInfoViewController - Show SAT scores for a particular school. Displayed when row is tapped in the SchoolListViewController

Supplementation View Controllers<br>
SchoolDetailViewController - Displays additional information about the school when the accessory view is tapped in SchoolListViewController<br>
SchoolSortOptionsViewController - Allows you to sort the data in SchoolListViewController<br>
SchoolMapViewController - Accessible from SchoolDetailViewController. Maps the school location using MapKit

DataSources<br>
SchoolDataSource - Base class for service calls<br>
SchoolInfoDataSource - Implements service call to get school list<br>
SchoolSATInfoDataSource - Implements service call get school SAT Data
   